<?php
ini_set( "display_errors", true );
date_default_timezone_set( "Russia/Moscow" );
define( "DB_DSN", "mysql:host=localhost;dbname=mycms" );
define( "DB_USERNAME", "" );
define( "DB_PASSWORD", "" );
define( "CLASS_PATH", "classes" );
define( "TEMPLATE_PATH", "templates" );
define( "HOMEPAGE_NUM_ARTICLES", 5 );
define( "ADMIN_USERNAME", "admin" );
define( "ADMIN_PASSWORD", "" );
require( CLASS_PATH . "/Article.php" );

function handleException( $exception ) {
  echo "Ничего не получилось";
  error_log( $exception->getMessage() );
}

//set_exception_handler( 'handleException' );
?>
